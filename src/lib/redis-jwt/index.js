import RedisJWT from 'redis-jwt';
import chalk from 'chalk';
import config from '../../config';

export const redisJwtClient = new RedisJWT(config['redis-jwt']);

export const exec = redisJwtClient.exec();

export const call = redisJwtClient.call();

export function connect() {

    return new Promise((resolve, reject) => {

        redisJwtClient.on('ready', () => {
            console.log(chalk.greenBright(`-------\nRedis-> connected on ${config['redis-jwt'].host}:${config['redis-jwt'].port}/${config['redis-jwt'].db}\n-------`));
            resolve();
        });

        redisJwtClient.on('error', (err) => {
            console.log(chalk.redBright(err));
        });
    });

}
