import winston from 'winston';
import config from '../../config';


let options = {
  console: {},
  errorFile: { filename: 'file_log_error.log' },
  file: { filename: 'file_log.log' }
}


let loggerX = winston.createLogger({
  transports: [
    new (winston.transports.Console)(options.console),
    new (winston.transports.File)(options.errorFile),
    new (winston.transports.File)(options.file)
  ],
  exitOnError: false, // do not exit on handled exceptions
});

// logger dev and production config
if (config.mode === 'development') {
  loggerX = winston.createLogger({
    transports: [
      new (winston.transports.Console)(options.console),
    ],
    exitOnError: false, // do not exit on handled exceptions
  });
} else if (config.mode === 'production') {
  loggerX = winston.createLogger({
    transports: [
      new (winston.transports.Console)(options.console),
      new (winston.transports.File)(options.errorFile),
      new (winston.transports.File)(options.file)
    ],
    exitOnError: false, // do not exit on handled exceptions
  });
}

export const logger = loggerX;
