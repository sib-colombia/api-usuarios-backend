import * as nodemailer from 'nodemailer';
import Promise from 'bluebird';
import config from '../../config';

// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport({service: 'gmail', auth: config.email.auth});

// send email
export function send(options) {
    return Promise.resolve(transporter.sendMail(options), transporter.close());
}
