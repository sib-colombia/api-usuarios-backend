'use strict';

// RESULT
export function result(res, statusCode, msg) {
      
  return (entity) => {
    if (entity)
      return res.status(statusCode || status.OK).json({
        "state" : true,
        "data" : entity,
        "message" : msg
      });
    return null;
  };
}


// RESULT
export function resultSimple(res, statusCode, msg) {

  return (entity) => {
    if (entity)
      return res.status(status.OK).json(entity);
    return null;
  }
    
}

// ERROR
export function error(res, statusCode, data) {
  return (err) => {
    if (err){
      console.log("error: ", error)
      return res.status(500).json({message: err});
    }
    return null;
  };
}

// NOTFOUND
export function notFound(res, statusCode, data) {

  return (entity) => {
    if (!entity) {
      res.status(statusCode || status.NOT_FOUND).end();
      return null;
    }
    return entity
  };
}

