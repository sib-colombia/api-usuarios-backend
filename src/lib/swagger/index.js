import swaggerTools from 'swagger-tools';
import express from 'express';
import {error} from 'express-easy-helper';
import errors from "../../utils/constants/errors";
import {mw} from '../../auth/services/mw.service';
import config from '../../config';
const {logger} = require('../../lib/log4js');

export async function index(app) {

    let swaggerConfig = await require('./config').default(app);

    let routerConfig = {
        controllers: [
            `${config.base}/api/controllers`,
            `${config.base}/auth/controllers`
        ],
        useStubs: false // If you want use examples.
    };

    return new Promise((resolve, reject) => {
        swaggerTools.initializeMiddleware(swaggerConfig, middleware => {
            // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
            app.use(middleware.swaggerMetadata());
            // Provide the security handlers
            app.use(middleware.swaggerSecurity({Bearer: mw}));
            // Validate Swagger requests
            app.use(middleware.swaggerValidator({validateResponse: false}));
            // Route validated requests to appropriate controller
            app.use(middleware.swaggerRouter(routerConfig));
            // Serve the Swagger documents and Swagger UI
            //   http://localhost:8000/docs => Swagger UI
            //   http://localhost:8000/api-docs => Swagger document
            // app.use(middleware.swaggerUi());
            app.use(errorHandler);
            router(app, swaggerConfig);
            resolve();
        });
    })
}

function errorHandler(err, req, res, next) {
    if (err.code === 'SCHEMA_VALIDATION_FAILED') {
        logger.error(JSON.stringify(err))
        let validationsErrors = {...err}
        validationsErrors.error = ""
        let errorObjectMissing = false
        let errorInvalidType = false
        let errorMaxLength = false
        let errorMinLength = false
        let errorMinimum = false
        let errorMaximum = false
        let errorMaximumDecimal = false
        for (let i = 0; i < err.results.errors.length; i++) {
            switch (err.results.errors[i].code) {
                case "OBJECT_MISSING_REQUIRED_PROPERTY":
                    errorObjectMissing = true
                    validationsErrors.error += errors.VALIDACIONES_CAMPOS.CABECERA_CAMPOS_OBLIGARIOS + " (" + err.results.errors[i].message.replace("Missing required property: ", "") + ").\n"
                    break
                case "INVALID_TYPE":
                    errorInvalidType = true
                    validationsErrors.error += errors.VALIDACIONES_CAMPOS.CABECERA_FORMATO_CAMPO_INVALIDO + " (" + err.results.errors[i].path.join(".") + ").\n"
                    break
                case "MAX_LENGTH":
                    errorMaxLength = true
                    validationsErrors.error += errors.VALIDACIONES_CAMPOS.CABECERA_MAXIMA_LONGITUD_CAMPO + " (" + err.results.errors[i].path.join(".") + ").\n"
                    break
                case "MIN_LENGTH":
                    errorMinLength = true
                    validationsErrors.error += errors.VALIDACIONES_CAMPOS.CABECERA_MINIMA_LONGITUD_CAMPO + " (" + err.results.errors[i].path.join(".") + ").\n"
                    break
                case "MINIMUM":
                    errorMinimum = true
                    validationsErrors.error += errors.VALIDACIONES_CAMPOS.CABECERA_MINIM0_CAMPO_NUMERICO + " (" + err.results.errors[i].path.join(".") + ").\n"
                    break
                case "MAXIMUM":
                    errorMaximum = true
                    validationsErrors.error += errors.VALIDACIONES_CAMPOS.CABECERA_MAXIMO_CAMPO_NUMERICO + " (" + err.results.errors[i].path.join(".") + ").\n"
                    break
                case "MULTIPLE_OF":
                    errorMaximumDecimal = true
                    validationsErrors.error += errors.VALIDACIONES_CAMPOS.CABECERA_MAXIMO_POSICIONES_DECIMALES + " (" + err.results.errors[i].path.join(".") + ").\n"
                    break
            }
        }
        validationsErrors.message = ""
        if (errorObjectMissing) {
            validationsErrors.message += errors.VALIDACIONES_CAMPOS.CAMPOS_OBLIGARIOS
        }
        if (errorInvalidType) {
            validationsErrors.message += errors.VALIDACIONES_CAMPOS.FORMATO_CAMPO_INVALIDO
        }
        if (errorMaxLength) {
            validationsErrors.message += errors.VALIDACIONES_CAMPOS.MAXIMA_LONGITUD_CAMPO
        }
        if (errorMinLength) {
            validationsErrors.message += errors.VALIDACIONES_CAMPOS.MINIMA_LONGITUD_CAMPO
        }
        if (errorMinimum) {
            validationsErrors.message += errors.VALIDACIONES_CAMPOS.MINIM0_CAMPO_NUMERICO
        }
        if (errorMaximum) {
            validationsErrors.message += errors.VALIDACIONES_CAMPOS.MAXIMO_CAMPO_NUMERICO
        }
        if (errorMaximumDecimal) {
            validationsErrors.message += errors.VALIDACIONES_CAMPOS.MAXIMO_POSICIONES_DECIMALES
        }
        delete validationsErrors.code != null ? delete validationsErrors.code : undefined
        delete validationsErrors.failedValidation != null ? delete validationsErrors.failedValidation : undefined
        delete validationsErrors.results != null ? delete validationsErrors.results : undefined
        delete validationsErrors.path != null ? delete validationsErrors.path : undefined
        delete validationsErrors.paramName != null ? delete validationsErrors.paramName : undefined
        return error(res, 400, validationsErrors)
    }
    if (!res.headersSent) {
        return error(res, err.message);
    }
}

function router(app, swaggerConfig) {
    // Remove Routers from /docs with property x-hide: true
    Object.keys(swaggerConfig.paths).forEach((keyParent, i) => {
        let parent = swaggerConfig.paths[keyParent];
        Object.keys(parent).forEach((keyChild, j) => {
            let child = swaggerConfig.paths[keyParent][keyChild]
            if (typeof child === 'object' && 'x-hide' in child && child['x-hide'] === true) {
                delete swaggerConfig.paths[keyParent][keyChild];
            }
        })
    })
    // or remove manual, exmaple:
    // delete swaggerConfig.paths['/auth/github'];
    // If Swagger is enabled then the router is enabled!
    if (config.swagger.enabled) {
        app.get(`/swagger.json`, (req, res) => res.json(swaggerConfig));
        app.use('/docs', express.static(`${config.base}/lib/swagger/ui`));
    }
};
