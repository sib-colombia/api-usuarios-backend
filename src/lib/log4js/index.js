import log4js from 'log4js';

log4js.configure({
  appenders: {
    usuarios_standar_out: {type: 'stdout'},
    usuarios_file: {type: 'file', filename: './usuarios.log'}
  },
  categories: {default: {appenders: ['usuarios_file','usuarios_standar_out'], level: 'info'}}
});

const logger = log4js.getLogger();

module.exports = {logger};
