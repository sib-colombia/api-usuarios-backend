export default `<!doctype html>
<html>

  <head>
	  <meta charset="UTF-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <title>{{title}}</title>

	  <style type="text/css">
	  </style>
  </head>

  <body>
    <ul>
      <li>Correo del usuario: {{email}}</li>
      <li>Link de recuperación: <a href="{{link}}">{{link}}</li>
    </ul>
  </body>
</html>`;
