import path from 'path';
const dotenv = require('dotenv');
dotenv.config();
const APP_NAME = process.env.APP_NAME
const DB_HOST = process.env.DB_HOST;
const DB_NAME = process.env.DB_NAME;
const API_USUARIOS = process.env.API_USUARIOS;
const API_DOMINIOS = process.env.API_DOMINIOS;
const CLIENT = '/client';

export default {
  api:{
    API_USUARIOS,
    API_DOMINIOS
  },
  secret: `caCcvgR32d77DhZz3CsuGbWQ`, // Secret Key
  server: { // Express
    ip: process.env.IP,
    port: process.env.PORT
  },
  passRecovery: {
    url :  process.env.URL_PASSWORD_RECOVERY
  },
  log: true, // show logs
  // Roles: if a user has multiple roles, will take the time of the greater role
  roles: [
    {
      role: 'user',
      ttl: '60 minutes',
    }, {
      role: 'admin',
      ttl: '5 days'
    }
  ],
  path: {
    disabled: '/:url(api|assets|auth|config|lib|views)/*' // paths 404
  },
  "socket.io": { // Socket.io
    port: process.env.SOCKET_IO_PORT, // public port listen, change also in views/default/demo.js
    example: true, // router -> http://localhost:8000/socket
    redis: { // Redis config
      host: process.env.REDIS_HOST, //can be IP or hostname
      port: process.env.REDIS_PORT, // port
    }
  },
  email: {
    auth :{
      user: process.env.EMAIL_PASSWORD_RECOVERY,
      pass: process.env.PASSWORD_EMAIL_RECOVERY
    }
  },
  "redis-jwt": { // Sessions
    //host: '/tmp/redis.sock', //unix domain
    host: process.env.REDIS_HOST, //can be IP or hostname
    port: process.env.REDIS_PORT, // port
    maxretries: 10, //reconnect retries, default 10
    //auth: '123', //optional password, if needed
    db: 0, //optional db selection
    secret: 'secret_key', // secret key for Tokens!
    multiple: true, // single or multiple sessions by user
    kea: false // Enable notify-keyspace-events KEA
  },
  mongoose: { // MongoDB
    // uri: mongodb://username:password@host:port/database?options
    uri: `${DB_HOST}`,
    options: {
    },
    seed: {
      path: '/api/models/seeds/',
      list: [
        {
          file: 'user.seed',
          schema: 'User',
          plant: 'once' //  once - always - never
        },
      ]
    },
  },
  swagger: { // Swagger
    enabled: true, // router -> http://localhost:8000/docs/
    info: {
      version: 'v2.0',
      title: APP_NAME,
      description: `RESTful API ${APP_NAME}`,
      "contact": {
        "name": "SiB Colombia - PIFS Backend",
        "url": "http://www.sibcolombia.net",
        "email": "sib@humboldt.com"
      },
      "license": {
        "name": "MIT",
        "url": "https://gitlab.com/sib-colombia/pifs-backend/blob/master/LICENSE"
      }
    }
  },
  oAuth: { // oAuth
    local: {
      enabled: true
    },
    facebook: {
      enabled: false,
      clientID: '',
      clientSecret: '',
      callbackURL: '/auth/facebook/callback'
    },
    twitter: {
      enabled: false,
      clientID: '',
      clientSecret: '',
      callbackURL: '/auth/twitter/callback'
    },
    google: {
      enabled: false,
      clientID: '',
      clientSecret: '',
      callbackURL: '/auth/google/callback'
    },
    github: {
      enabled: true,
      clientID: '52be92c9a41f77a959eb',
      clientSecret: '76c9bb03c689d098506822fa80dba372a1fe29c8',
      callbackURL: '/auth/github/callback'
    }
  },
  // globals
  mode: process.env.NODE_ENV || 'development', // mode
  name: APP_NAME, // name
  node: parseInt(process.env.NODE_APP_INSTANCE) || 0, // node instance
  root: path.normalize(`${__dirname}/../..`), // root
  base: path.normalize(`${__dirname}/..`), // base
  client: `${path.normalize(`${__dirname}/../..`)}${CLIENT}`, // client
};
