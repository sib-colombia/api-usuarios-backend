import mongoose from 'mongoose';
import _ from 'lodash'

import Rol from '../models/rol.model';

var ObjectId = mongoose.Types.ObjectId;

export function list(req, res) {

  Rol.find().populate("system").populate("permissions").exec().then(roles => {
    res.json(roles);
  }).catch(err => {
    res.status(500).json({error:err});
  });
}

export function search(req, res) {

  Rol.find(req.body).populate("system").populate("permissions").exec().then(roles => {
    res.json(roles);
  }).catch(err => {
    console.log(err)
    res.status(500).json({error:err});
  });
}

export function read(req, res) {

  Rol.findById(req.swagger.params.id.value).populate("system").populate("permissions").exec().then(result => {
    res.json(result);
  }).catch(err => {
    res.status(500).json({error:err});
  });
}

export function create(req, res) {

  let create = new Rol();

  create.name = req.body.name;
  create.description = req.body.description;
  create.active = req.body.active;
  create.system = ObjectId(req.body.system);
  let permissions = [];
  _.forEach(req.body.permissions, (p, k)=>{
    permissions.push(ObjectId(p));
  })
  create.permissions = permissions

  create.save().then(result => {
    res.json(result);
  }).catch(err => {
    console.log(err)
    res.status(500).json({error:err});
  })

}

export function update(req, res) {
  let permissions = [];
  _.forEach(req.body.permissions, (p, k)=>{
    permissions.push(ObjectId(p));
  })
  Rol.findByIdAndUpdate(req.swagger.params.id.value, {
    $set: {
      name: req.body.name,
      description: req.body.description,
      active: req.body.active,
      permissions: permissions,
      system: ObjectId(req.body.system),
    }
  }, {
    new: true
  }).exec().then(result => {
    res.json(result);
  }).catch(err => {
    res.status(500).json({error:err});
  });

}

export function remove(req, res) {

  Rol.deleteOne({
    _id: req.swagger.params.id.value
  }).then(result => {
    res.json(result);
  }).catch(err => {
    res.status(500).json({error:err});
  });

}
