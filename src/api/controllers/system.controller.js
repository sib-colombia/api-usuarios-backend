import System from '../models/system.model';

export function list(req, res) {

  System.find().exec().then(greets => {
    res.json(greets);
  }).catch(err => {
    res.status(500).json({error:err});
  });
}

export function read(req, res) {

  System.findById(req.swagger.params.id.value).exec().then(result => {
    res.json(result);
  }).catch(err => {
    res.status(500).json({error:err});
  });

}

export function create(req, res) {

  console.log("Creando sistema: ", req.body)

  let create = new System();

  create.name = req.body.name;
  create.description = req.body.description;
  create.url = req.body.url;
  create.active = req.body.active;

  create.save().then(result => {
    res.json(result);
  }).catch(err => {
    res.status(500).json({error:err});
  })

}

export function update(req, res) {

  System.findByIdAndUpdate(req.swagger.params.id.value, {
    $set: {
      name: req.body.name,
      description: req.body.description,
      url: req.body.url,
      active: req.body.active,
    }
  }, {
    new: true
  }).exec().then(result => {
    res.json(result);
  }).catch(err => {
    res.status(500).json({error:err});
  });

}

export function remove(req, res) {

  System.deleteOne({
    _id: req.swagger.params.id.value
  }).then(result => {
    res.json(result);
  }).catch(err => {
    res.status(500).json({error:err});
  });

}
