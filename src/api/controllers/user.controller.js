import randtoken from 'rand-token'
import bcrypt from 'bcrypt'
import _ from 'lodash'
import {result, notFound, error} from 'express-easy-helper';
const {validatePassword, loggerAudit} = require('../../utils/tools')
import * as utility from '../../lib/utility'
import * as email from '../../lib/email'
const EmailTemplate = utility.getTemplate('email/passwordRecovery.js')
const EmailTemplatePlain = utility.getTemplate('email/passwordRecoveryPlain.js')
import {validateExistRols} from '../../utils/rolUtils'
import User from '../models/user.model'
import System from '../models/system.model'
import Rol from '../models/rol.model'
import messages from "../../utils/constants/messages";
import errors from "../../utils/constants/errors";
const {logger} = require('../../lib/log4js');
import config from '../../config';

export async function create(req, res) {
    Promise.resolve()
        .then(() => {
            let user = new User();
            user.username = req.body.username;
            user.name = req.body.name;
            user.lastname = req.body.lastname;
            user.email = req.body.email;
            user.document = req.body.document;
            user.documentType = req.body.documentType;
            user.telephone = req.body.telephone;
            user.address = req.body.address;
            user.status = req.body.status;
            return user
        })
        .then(async (user) => {
            let roles = req.body.roles.filter((elem, index, self) => {
                return index == self.indexOf(elem)
            });
            let validRols = await validateExistRols(roles)

            if (validRols) {
                roles = await complementRols(roles)
                user.roles = roles
                if (req.body.password != req.body.verify) {
                    return Promise.reject({
                        code: 400,
                        message: errors.usuario.ERROR_CONFIRMACION_CONTRASENA
                    })
                }
                if (!validatePassword(req.body.password)) {
                    return Promise.reject({
                        code: 400,
                        message: errors.usuario.ERROR_FORMATO_CONTRASENA
                    })
                }

                user.password = req.body.password;

                return new Promise((resolve, reject) => {
                    bcrypt.genSalt(10, (err, salt) => {
                        if (err) {
                            return reject({
                                code: 500,
                                message: errors.usuario.ERROR_GENERAR_HASH_CONTRASENA,
                                stack: err.stack
                            })
                        } else {
                            return bcrypt.hash(user.password, salt, (err, hash) => {
                                if (err) {
                                    return reject({
                                        code: 500,
                                        message: errors.usuario.ERROR_GENERAR_HASH_CONTRASENA,
                                        stack: err.stack
                                    })
                                } else {
                                    user.password = hash;
                                    user.save()
                                        .then(result => {
                                            return res.json({
                                                'message': messages.usuario.CREACION_USUARIO_SATISFACTORIA,
                                                'data': result
                                            })
                                        }).catch(err => {
                                        return reject({
                                            code: 500,
                                            message: err.message,
                                            stack: err.stack
                                        })
                                    })
                                }
                            });
                        }
                    });
                })
            } else {
                return Promise.reject({
                    code: 400,
                    message: errors.rol.ERROR_ROL_INEXISTENTE
                })
            }
        })
        .catch((err) => {
            logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
            logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
            return res.status(err.code != null ? err.code : 500).json({
                message: errors.usuario.ERROR_CREAR_USUARIO,
                error: err.message
            })
        })
}

function complementRols(userRols, req) {
    let complementedRols = []
    return Promise
        .resolve(complementedRols)
        .then((complementedRols) => {
            return Rol.find()
                .exec()
                .then((systemRols) => {
                    return {systemRols, complementedRols}
                })
        })
        .then(async ({systemRols, complementedRols}) => {
            await Promise.all(userRols.map((rol) => {
                    complementedRols.push(rol)
                    return Rol.findOne({_id: rol})
                        .exec()
                        .then((currentRol) => {
                            return {currentRol, systemRols, complementedRols}
                        })
                        .then(({currentRol, systemRols, complementedRols}) => {
                            if (currentRol.name === 'ADMINISTRADOR DEL SISTEMA') {
                                complementedRols.push(_.find(systemRols, {"name": "ADMINISTRADOR USUARIOS SISTEMA"})._id)
                                complementedRols.push(_.find(systemRols, {"name": "ADMINISTRADOR DE DOMINIOS"})._id)
                            }
                            return {systemRols, complementedRols}
                        })
                        .then(({systemRols, complementedRols}) => {
                            complementedRols.push(_.find(systemRols, {name: "USUARIO PIFS GENÉRICO"})._id)
                            complementedRols.push(_.find(systemRols, {name: "CONSUMIDOR DE DOMINIOS"})._id)
                            complementedRols = complementedRols.filter((elem, index, self) => {
                                return index == self.indexOf(elem)
                            });
                        }).catch((err) => {
                            return Promise.reject({
                                code: 500,
                                message: err.message,
                                stack: err.stack
                            })
                        })
                })
            );
            return complementedRols
        }).catch((err) => {
            throw err
        })
}

// Update a user
export function updateUserByAdmin(req, res) {
    User.findOne({"username": req.swagger.params.username.value})
        .then(async user => {
            let roles = req.body.roles.filter((elem, index, self) => {
                return index == self.indexOf(elem)
            });
            let validRols = await validateExistRols(roles)

            if (validRols) {
                var totalRols = await complementRols(roles)
                return new Promise((resolve, reject) => {
                    if (req.body.password) {
                        if (!validatePassword(req.body.password)) {
                            return reject({
                                code: 400,
                                message: errors.usuario.ERROR_FORMATO_CONTRASENA
                            })
                        }
                        bcrypt.genSalt(10, (err, salt) => {
                            if (err) {
                                return reject({
                                    code: 500,
                                    message: errors.usuario.ERROR_GENERAR_HASH_CONTRASENA,
                                    stack: err.stack
                                })
                            }
                            bcrypt.hash(req.body.password, salt, (err, hash) => {
                                if (err) {
                                    return reject({
                                        code: 500,
                                        message: errors.usuario.ERROR_GENERAR_HASH_CONTRASENA,
                                        stack: err.stack
                                    })
                                } else {
                                    req.body.password = hash;
                                    return User.findOneAndUpdate(
                                        {username: req.swagger.params.username.value}, {
                                            $set: {
                                                name: req.body.name,
                                                email: req.body.email,
                                                password: req.body.password,
                                                lastname: req.body.lastname,
                                                roles: totalRols,
                                                document: req.body.document,
                                                documentType: req.body.documentType,
                                                telephone: req.body.telephone,
                                                address: req.body.address,
                                                status: req.body.status
                                            }
                                        }, {
                                            new: true
                                        }, (err, doc) => {
                                            if (err) {
                                                return reject({
                                                    code: 500,
                                                    message: errors.usuario.ERROR_GENERAR_HASH_CONTRASENA,
                                                    stack: err.stack
                                                })
                                            } else {
                                                return resolve(doc)
                                            }
                                        })
                                }
                            });
                        });
                    } else {
                        return User.findOneAndUpdate(
                            {username: req.swagger.params.username.value}, {
                                $set: {
                                    name: req.body.name,
                                    email: req.body.email,
                                    lastname: req.body.lastname,
                                    roles: totalRols,
                                    document: req.body.document,
                                    documentType: req.body.documentType,
                                    telephone: req.body.telephone,
                                    address: req.body.address,
                                    status: req.body.status
                                }
                            }, {
                                new: true
                            }).exec()
                            .then(notFound(res))
                            .then((res) => {
                                return resolve(res)
                            })
                            .catch(err => {
                                return reject({
                                    code: 500,
                                    message: errors.usuario.ERROR_GENERAR_HASH_CONTRASENA,
                                    stack: err.stack
                                })
                            })
                    }
                })
            } else {
                logger.error(loggerAudit(errors.rol.ERROR_ROL_INEXISTENTE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
                return Promise.reject({
                        "code": 400,
                        "message": errors.rol.ERROR_ROL_INEXISTENTE
                    }
                )
            }
        })
        .then((updatedUser) => {
            return res.status(200).json({
                message: messages.usuario.ACTUALIZACION_SATISFACTORIA,
                data: updatedUser
            })
        })
        .catch(err => {
            logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
            logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_DETALLE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
            return res.status(err.code != null ? err.code : 500).json({
                "message": errors.usuario.ERROR_ACTUALIZACION_USUARIO,
                "error": err.message
            })
        });
}

export function updateOwnUser(req, res) {
    console.log(req.user.username)
    User.findOne({"username": req.user.username})
        .then(user => {
            user.name = req.body.name;
            user.lastname = req.body.lastname;
            user.document = req.body.document;
            user.documentType = req.body.documentType;
            user.telephone = req.body.telephone;
            user.address = req.body.address;
            user.email = req.body.email;
            user.save().then(result => {
                res.status(200).json({message: messages.usuario.ACTUALIZACION_SATISFACTORIA, data: result})
            }).catch(err => {
                logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
                logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
                res.status(500).json({"message": errors.usuario["ERROR_ACTUALIZACION_CONTRASENA"], "error": err})
            });
        })
        .catch(err => {
            logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
            logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
            res.status(500).json({error: err})
        });
}

export function list(req, res) {
    User.find({}, {
        select: '-password',
        page: parseInt(req.swagger.params.page.value),
        limit: parseInt(req.swagger.params.quantity.value)
    }).then((users) => {
        res.json(users)
    });
}

export function read(req, res) {
    User.findOne({"username": req.swagger.params.username.value}, {
        password: 0,
        social: 0
    }).populate("roles").exec().then(result => {
        res.status(200).json({data: result, message: "La búsqueda se realizo satisfactoriamente."});
    }).catch(err => {
        logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
        logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
        res.status(500).json({message: errors.usuario.ERROR_BUSCAR_USUARIO, error: err.message})
    });
}

export function me(req, res) {
    try {
        let user = req.user;
        delete user.session.id;
        return result(res, user);
    } catch (err) {
        logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
        logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
        res.status(500).json({error: err.message});
    }
}

export function recovery(req, res) {
    User.findOne({"email": req.body.email}).exec().then(user => {
            if (!user) {
                logger.error(loggerAudit(errors.usuario.ERROR_CORREO_NO_ENCONTRADO, req.user == null ? messages.general.NOT_LOGIN : req.user.username));
                return res.status(500).json({
                    message: errors.usuario.ERROR_RECUPERACION_CONTRASENA,
                    error: errors.usuario.ERROR_CORREO_NO_ENCONTRADO
                })
            }

            let mailOptions = {
                to: req.body.email,
                subject: messages.general.CABECERA_CORREO_RECUPERACIÓN_CONTRASEÑA,
                text: '',
                html: ''
            };

            user.token = randtoken.uid(32)
            let link = config.passRecovery.url + user.token

            user.save().then(result => {
                    var values = {
                        email: req.body.email,
                        link: link,
                        title: messages.general.CABECERA_CORREO_RECUPERACIÓN_CONTRASEÑA
                    };

                    EmailTemplatePlain.then(templatePlain => {
                        EmailTemplate.then(template => {
                            mailOptions.text = utility.setTemplate(templatePlain, values);
                            mailOptions.html = utility.setTemplate(template, values);
                            email.send(mailOptions).then(result => {
                                res.status(200).json({"message": messages.usuario.ENVIO_ENLACE_RECUPERACION});
                            }).catch(err => {
                                    logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username));
                                    logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username));
                                    return res.status(500).json({
                                        "message": errors.usuario.ERROR_CORREO_RECUPERACION_CONTRASENA,
                                        "error": err.message
                                    })
                                }
                            );
                        });
                    })
                }
            ).catch(err => {
                logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username));
                logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username));
                res.status(500).json({error: err})
            })
        }
    ).catch(err => {
        logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username));
        logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username));
        res.status(500).json({error: err})
    });
}

export function token(req, res) {
    if (req.body.token !== "") {
        User.findOne({"token": req.body.token}).exec().then(user => {
            if (user === null) {
                return res.status(400).json({"message": errors.usuario.ERROR_TOKEN_NO_VALIDO})
            }
            if (req.body.password != req.body.verify) {
                return res.status(400).json({"message": errors.usuario.ERROR_CONFIRMACION_CONTRASENA})
            }
            user.password = req.body.password;
            user.token = "";

            bcrypt.genSalt(10, (err, salt) => {
                if (err) {
                    logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
                    logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
                    return next(err);
                }

                bcrypt.hash(user.password, salt, (err, hash) => {
                    if (err) {
                        logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
                        logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
                        res.status(500).json({
                            message: errors.usuario.CREACION_USUARIO_SATISFACTORIA,
                            error: err.message
                        });
                    }
                    user.password = hash;
                    user.save().then(result => {
                        res.status(200).json({
                            message: messages.usuario.ACTUALIZACION_CONTRASENA_CORRECTA
                        });
                    }).catch(err => {
                        logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
                        logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
                        res.status(500).json({
                            message: errors.usuario.CREACION_USUARIO_SATISFACTORIA,
                            error: err.message
                        })
                    })
                });
            });
        }).catch(err => {
            logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
            logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
            res.status(500).json({
                "message": errors.usuario.CREACION_USUARIO_SATISFACTORIA,
                "error": err.message
            })
        })
    } else {
        res.status(404).json({"message": errors.usuario.TOKEN_INEXISTENTE_PETICION})
    }
}

export function permission(req, res) {
    User.findOne({"username": req.body.username}, {
        password: 0,
        social: 0
    }).populate("roles").populate(
        {
            path: 'roles',
            populate: {
                path: 'permissions',
                model: 'Permission'
            }
        }
    ).exec().then(result => {
        let respuesta = false
        _.forEach(result.roles, (r, k) => {
            _.forEach(r.permissions, (p, k2) => {
                if (p.url === req.body.url) {
                    if (p.actions[req.body.action]) {
                        respuesta = true
                    }
                }
            })
        });
        res.json({"response": respuesta, "user": result});
    }).catch(err => {
        logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
        logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
        res.status(500).json({error: err})
    });
}

export function find(req, res) {
    User.findOne({"username": req.body.username}, {
        password: 0,
        social: 0
    }).populate("roles").populate(
        {
            path: 'roles',
            populate: {
                path: 'permissions',
                model: 'Permission'
            }
        }
    ).exec().then(result => {
        res.json({"user": result});
    }).catch(err => {
        logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
        logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
        res.status(500).json({error: err.message})
    });
}

export function findUsersBySystem(req, res) {
    System.findOne({"name": req.body.system}).exec().then(result => {
        Rol.find({"system": result._id}).exec().then(
            result => {
                User.find({"roles": {$in: result}}, {
                    password: 0,
                    social: 0
                }).populate("roles").populate(
                    {
                        path: 'roles',
                        populate: {
                            path: 'permissions',
                            model: 'Permission'
                        }
                    }
                ).exec().then(result =>
                    res.json({"users": result})
                )
            }
        )
    }).catch(err => {
        logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
        logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
        res.status(500).json({error: err})
    });
}

// List of user's
export function listAdmin(req, res) {
    return User.find({})
        .select('-social')
        .exec()
        .then(notFound(res))
        .then(result(res))
        .catch((err) => {
            logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
            logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_TRAZA, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
            error(err)
        });
}
