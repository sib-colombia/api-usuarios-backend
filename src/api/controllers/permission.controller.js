import Permission from '../models/permission.model';
import mongoose from 'mongoose';
var ObjectId = mongoose.Types.ObjectId;

export function list(req, res) {
  Permission.find().exec().then(greets => {
    res.json(greets);
  }).catch(err => {
    res.status(500).json({error:err});
  });
}

export function read(req, res) {
  Permission.findById(req.swagger.params.id.value).populate("system").exec().then(result => {
    res.json(result);
  }).catch(err => {
    res.status(500).json({error:err});
  });
}

export function create(req, res) {
  let create = new Permission();
  create.name = req.body.name;
  create.description = req.body.description;
  create.url = req.body.url;
  create.active = req.body.active;
  create.actions = req.body.actions;
  create.system = ObjectId(req.body.system);
  create.save().then(result => {
    res.json(result);
  }).catch(err => {
    console.log(err)
    res.status(500).json({error:err});
  })
}

export function update(req, res) {
  Permission.findByIdAndUpdate(req.swagger.params.id.value, {
    $set: {
      name: req.body.name,
      description: req.body.description,
      url: req.body.url,
      active: req.body.active,
      actions: req.body.actions,
      system: ObjectId(req.body.system),
    }
  }, {
    new: true
  }).exec().then(result => {
    res.json(result);
  }).catch(err => {
    res.status(500).json({error:err});
  });
}

export function remove(req, res) {
  Permission.deleteOne({
    _id: req.swagger.params.id.value
  }).then(result => {
    res.json(result);
  }).catch(err => {
    res.status(500).json({error:err});
  });
}
