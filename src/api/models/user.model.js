import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';
const Schema = mongoose.Schema;
const idvalidator = require('mongoose-id-validator');

const UserSchema = new Schema({
  username: {
    type: String,
    required: [
      true, 'El usuario es requerido.'
    ],
    unique: true
  },
  password: {
    type: String,
    select: false
  },
  name: {
    type: String
  },
  lastname: String,
  email: {
    type: String,
    required: [
      true, 'El correo electrónico es requerido.'
    ],
    unique: [
        true,
        'El email ya esta registrado en el sistema.'
    ],
    lowercase: true
  },
  photo: String,
  provider: {
    type: String,
    required: [
      true, 'Provider is required.'
    ],
    default: 'local'
  },
  roles: [
    { type : mongoose.Schema.ObjectId, ref: 'Rol' }
  ],
  status: {
    type: Boolean,
    default: true,
    required: [true, 'Status is required.']
  },
  date: {
    type: Date,
    default: Date.now
  },
  lastLogin: {
    type: Date
  },
  social: {
    id: String,
    info: {}
  },
  document: {
    type: String,
    unique: true
  },
  documentType: {
    type: Number,
  },
  token: {
    type: String,
  },
  telephone: {
    type: String,
  },
  address: {
    type: String,
  }
});

UserSchema.path('username').index({ unique: true });

UserSchema.plugin(mongoosePaginate);
UserSchema.plugin(idvalidator);

require('./hooks/user.hook').default(UserSchema);
require('./statics/user.static').default(UserSchema);
require('./methods/user.method').default(UserSchema);

export default mongoose.model('User', UserSchema);
