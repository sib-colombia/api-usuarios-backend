import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';
const Schema = mongoose.Schema;

const RolSchema = new Schema({
  name: {
    type: String,
    required: [true, 'El nombre es requerido.']
  },
  description: {
    type: String,
    required: [true, 'La descripción es requerida.']
  },
  active: {
    type: Boolean,
  },
  permissions: [
    { type : mongoose.Schema.ObjectId, ref: 'Permission' }
  ],
  system: {
    type : mongoose.Schema.ObjectId, ref: 'System'
  },
});

RolSchema.plugin(mongoosePaginate);
export default mongoose.model('Rol', RolSchema);
