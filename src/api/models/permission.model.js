import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';
const Schema = mongoose.Schema;

const PermissionSchema = new Schema({
  name: {
    type: String,
    required: [true, 'El nombre es requerido.']
  },
  description: {
    type: String,
    required: [true, 'La descripción es requerida.']
  },
  url: {
    type: String,
    required: [true, 'La url es requerida.']
  },
  active: {
    type: Boolean,
  },
  actions: {
    post: { type: Boolean },
    get: { type: Boolean },
    put: { type: Boolean },
    delete: { type: Boolean },
  },
  system: {
    type : mongoose.Schema.ObjectId, ref: 'System'
  },
});

PermissionSchema.plugin(mongoosePaginate);
export default mongoose.model('Permission', PermissionSchema);
