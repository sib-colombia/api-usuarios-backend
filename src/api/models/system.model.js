import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';
const Schema = mongoose.Schema;

const SystemSchema = new Schema({
  name: {
    type: String,
    required: [true, 'El nombre es requerido.']
  },
  description: {
    type: String,
    required: [true, 'La descripción es requerida.']
  },
  url: {
    type: String,
    required: [true, 'La url es requerida.']
  },
  active: {
    type: Boolean,
  }
});

SystemSchema.plugin(mongoosePaginate);
export default mongoose.model('System', SystemSchema);
