import mongoose from 'mongoose'
import _ from 'lodash'
import errors from "../../../utils/constants/errors";
import Rol from '../rol.model';
import {logger} from '../../../lib/log4js'
import {validateExistSystem} from  '../../../utils/systemUtils'

export default (User) => {

    User.statics = {
        loginByLocal(username, password, system) {
            return new Promise((resolve, reject) => {
                const User = this;
                User.findOne({
                    username: username,
                    provider: 'local'
                }).select("+password").populate('roles').exec().then(async (user) => {
                    if (!user)
                        reject({
                            message: errors.usuario.ERROR_AUTENTICAR_USUARIO + errors.usuario.ERROR_USUARIO_NO_REGISTRADO.replace("%", username),
                            error: errors.usuario.ERROR_USUARIO_NO_REGISTRADO.replace("%", username)
                        });
                    let rs = []
                    _.forEach(user.roles, (r, k) => {
                        rs.push(mongoose.Types.ObjectId(r._id))
                    })
                    let existSystem = await validateExistSystem(system)
                    if(existSystem){
                        Rol.find({'_id': {$in: rs}}).populate('permissions').populate('system').exec().then(roles => {
                            rs = []
                            _.forEach(roles, (r, k) => {
                                if (r.system.name === system || r.system.name === 'USUARIOS' || r.system.name === 'DOMINIOS') {
                                    rs.push(r)
                                }
                            })
                            user.roles = rs
                            if (rs.length == 0) {
                                reject(errors.usuario.ERROR_USUARIO_SIN_ROLES)
                            } else {
                                user.authenticate(password).then(isMatch => { // validate password
                                    if (!isMatch)
                                        reject({
                                            message: errors.usuario.ERROR_AUTENTICAR_USUARIO + errors.usuario.ERROR_CONTRASENA_INVALIDA,
                                            error: errors.usuario.ERROR_CONTRASENA_INVALIDA
                                        });
                                    if (!user.status)
                                        reject({
                                            message: errors.usuario.ERROR_AUTENTICAR_USUARIO + errors.usuario.ERROR_USUARIO_INACTIVO,
                                            error: errors.usuario.ERROR_USUARIO_INACTIVO
                                        })
                                    user.lastLogin = Date.now();
                                    user.save().then(_user => resolve(_user)).catch(err => {
                                        logger.error(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE)
                                        logger.error(err.stack != null ? err.stack : errors.general.ERROR_SIN_DETALLE)
                                        reject(err)
                                    });
                                });
                            }
                        })
                    }else{
                        reject({
                            message: errors.usuario.ERROR_AUTENTICAR_USUARIO + errors.usuario.ERROR_SISTEMA_NO_REGISTRADO,
                            error: errors.usuario.ERROR_SISTEMA_NO_REGISTRADO
                        })
                    }
                }).catch(err => {
                    logger.error(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE)
                    logger.error(err.stack != null ? err.stack : errors.general.ERROR_SIN_DETALLE)
                    reject(err)
                });
            });
        },

        loginBySocial(provider, profile) {
            return new Promise((resolve, reject) => {
                const User = this;
                User.findOne({
                    provider,
                    'social.id': profile.id
                }).exec().then(user => {
                    if (!user) {
                        user = new User({
                            provider: provider,
                            name: profile.displayName,
                            username: profile.username,
                            email: profile.email || '',
                            photo: profile.photo || '',
                            'social.id': profile.id,
                            'social.info': profile._json
                        });
                    } else {
                        user.social.info = profile._json;
                        user.photo = profile.photo || '';
                    }
                    user.lastLogin = Date.now();
                    user.save().then(_user => resolve(_user)).catch(err => {
                        logger.error(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE)
                        logger.error(err.stack != null ? err.stack : errors.general.ERROR_SIN_DETALLE)
                        reject(err)
                    });
                }).catch(err => {
                    logger.error(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE)
                    logger.error(err.stack != null ? err.stack : errors.general.ERROR_SIN_DETALLE)
                    reject(err)
                });
            });
        },

        findOneByUsername(username) {
            return new Promise((resolve, reject) => {
                this.findOne({username}).count().exec().then(found => {
                    resolve(found);
                }).catch(err => {
                    logger.error(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE)
                    logger.error(err.stack != null ? err.stack : errors.general.ERROR_SIN_DETALLE)
                    reject(err)
                });
            });
        }
    }
};
