import { result, invalid, error } from 'express-easy-helper';
import { calc, time } from 'role-calc';
import _ from 'lodash'
import errors from '../../utils/constants/errors'
import { redisJwtClient } from '../../lib/redis-jwt';
import config from '../../config';

// Initialize after login success
export async function initialize(err, user, res) {
  try {
    if (err)
      return invalid(res, {
        message: err.message,
        error: err.error
      });
    if (!user)
      return error(res, { message: errors.usuario.ERROR_AUTENTICAR_USUARIO});

    let lista = _.map(user.roles, "name");
    let ttl = calc(time(config.roles, lista), 'max');

    // Create session in redis-jwt
    const token = await redisJwtClient.sign(user.username.toString(), {
      ttl,
      dataSession: {// save data in REDIS (Private)
        ip: res.req.headers['x-forwarded-for'] || res.req.connection.remoteAddress,
        agent: res.req.headers['user-agent'],
        roles: user.roles,
        status: user.status,
        username: user.username,
        email: user.email
      },
      dataToken: {// save data in Token (Public)
        example: 'I travel with the token!'
      }
    });

    // Save token in cookies
    res.cookie('token', JSON.stringify(token));

    // if local return token
    if (user.provider === 'local')
      return result(res, { token });

    // if Social redirect to..
    res.redirect(`/?token=${token}`);

  } catch (err) {
    return error(res, { message: err });
  }
}
