import {unauthorized, forbidden} from 'express-easy-helper';
import errors from "../../utils/constants/errors";
import {redisJwtClient} from '../../lib/redis-jwt';
import User from '../../api/models/user.model';

// VerifyToken
export async function mw(req, authOrSecDef, token, cb) {
  let requiredRoles = req.swagger.operation["x-security-scopes"];
  if (token) {
    req.headers.authorization = `Bearer ${token}`;
    if (token.includes('Bearer')) {
      token = token.replace("Bearer ", "")
    }
    let session = undefined;
    try {
      session = await redisJwtClient.verify(token, true);
      if (!session)
        return cb(req.res.status(401).json({
          "message": errors.usuario["ERROR_MIDDLEWARE_USUARIO"],
          "error": errors.usuario["ERROR_SESION_INACTIVA"]
        }));
    } catch (error) {
      return cb(req.res.status(401).json({"message": errors.usuario["ERROR_BUSCAR_SESION"], "error": error.messages}));
    }
    let user = await User.findOne({'username': session.id}).select('-social').exec();
    if (!user)
      return cb(req.res.status(401).json({
        "message": errors.usuario["ERROR_MIDDLEWARE_USUARIO"],
        "error": errors.usuario["ERROR_USUARIO_NO_ENCONTRADO"]
      }));
    if (user.username !== session.id)
      return cb(req.res.status(401).json({
        "message": errors.usuario["ERROR_MIDDLEWARE_USUARIO"],
        "error": errors.usuario["ERROR_SESION_CORRUPTA"]
      }));
    if (!user.status)
      return cb(req.res.status(401).json({
        "message": errors.usuario["ERROR_MIDDLEWARE_USUARIO"],
        "error": errors.usuario["ERROR_USUARIO_INACTIVO"]
      }));
    let hasPermision = hasPermission(req, session.dataSession.roles)
    if (!requiredRoles || !hasPermision)
      return cb(req.res.status(403).json({
        "message": errors.usuario["ERROR_MIDDLEWARE_USUARIO"],
        "error": errors.usuario["ERROR_PERMISOS_INSUFICIENTES"]
      }));
    req.user = Object.assign({session}, user._doc);
    return cb(null);
  } else {
    return cb(req.res.status(401).json({
      "message": errors.usuario["ERROR_MIDDLEWARE_USUARIO"],
      "error": errors.usuario["ERROR_CABECERA_SIN_TOKEN"]
    }));
  }
}

// hasPermission
export function hasPermission(req, roles) {
  try {
    if (roles === undefined) {
      return false
    }
    let isAuthorized = false;
    roles.forEach(rol => {
      rol.permissions.forEach(permission => {
        let parts = permission.url.split("/")
        let reqParts = req.originalUrl.split("/")
        if (parts.length === reqParts.length) {
          let check = true
          for (let i = 0; i < parts.length; i++) {
            if (parts[i] !== reqParts[i]) {
              if (!parts[i].startsWith(":")) {
                check = false
              }
            }
          }
          if (check) {
            let r = (permission.actions.get && req.originalMethod === "GET") ||
                (permission.actions.post && req.originalMethod === "POST") ||
                (permission.actions.put && req.originalMethod === "PUT") ||
                (permission.actions.delete && req.originalMethod === "DELETE")
            if (r) {
              isAuthorized = true
            }
          }
        }
      })
    });
    return isAuthorized
  } catch (err) {
    return false;
  }
}
