import passport from 'passport';
import {Strategy as LocalStrategy} from 'passport-local';
import User from '../../api/models/user.model';
import {logger} from '../../lib/log4js'
import errors from "../../utils/constants/errors";
import messages from "../../utils/constants/messages";
const {loggerAudit} = require('../../utils/tools')

passport.use('local', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, function (req, username, password, done) {
    logger.info("Usuario intento login: ", username, password, req.swagger.params)
    User.loginByLocal(username, password, req.swagger.params.system.value)
        .then(user => {
            req.user = user
            done(null, user)
        })
        .catch(err => {
            logger.error(loggerAudit(err.message != null ? err.message : errors.general.ERROR_SIN_MENSAJE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
            logger.error(loggerAudit(err.stack != null ? err.stack : errors.general.ERROR_SIN_DETALLE, req.user == null ? messages.general.NOT_LOGIN : req.user.username))
            done(err)
        });
}));
