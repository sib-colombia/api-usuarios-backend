import Rol from "../api/models/rol.model";

export async function validateExistRols(roles) {
    return new Promise(async (resolve, reject) => {
        let valid = true
        try {
            for (var i = 0; i < roles.length; i++) {
                valid = await validateExistRol(roles[i])
                if (!valid) {
                    break
                }
            }
            resolve(valid)
        } catch (err) {
            return reject({
                code: 500,
                message: err.message,
                stack: err.stack
            })
        }
    }).then((result) => {
        return result
    }).catch((error) => {
        throw error
    })
}

async function validateExistRol(rolId) {
    return new Promise((resolve, reject) => {
        Rol.find({_id: rolId, active: true}).exec(async function (err, result) {
            if (err) {
                reject({
                    code: 500,
                    message: err.message,
                    stack: err.stack
                })
            } else {
                if (result.length != 0) {
                    resolve(true)
                } else {
                    resolve(false)
                }
            }
        });
    }).then((result) => {
        return result
    }).catch((err) => {
        throw err
    })
}
