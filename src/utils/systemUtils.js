import System from "../api/models/system.model";

export async function validateExistSystem(nameSystem) {
  return new Promise((resolve, reject) => {
    System.find({name: nameSystem, active: true}).exec(async function (err, result) {
      if (err) {
        reject({
          code: 500,
          message: err.message,
          stack: err.stack
        })
      } else {
        if (result.length != 0) {
          resolve(true)
        } else {
          resolve(false)
        }
      }
    });
  }).then((result) => {
    return result
  }).catch((err) => {
    throw err
  })
}
