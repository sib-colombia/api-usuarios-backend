export default {
    "usuario": {
        "ERROR_API_USUARIOS": "Error en API USUARIOS.",
        "ERROR_AUTENTICAR_USUARIO": "Se ha presentado un error al intentar autenticar el usuario.",
        "ERROR_USUARIO_NO_REGISTRADO" : "El usuario % no esta registrado en el sistema.",
        "ERROR_USUARIO_SIN_ROLES" : "El usuario no posee roles.",
        "ERROR_SISTEMA_NO_REGISTRADO" : "El sistema contra el cual desea autenticarse no existe.",
        "ERROR_ACTUALIZACION_USUARIO" : "Error al actualizar el usuario.",
        "ERROR_GENERAR_HASH_CONTRASENA": "Error al generar la seguridad de la contraseña.",
        "ERROR_CONTRASENA_INVALIDA" : "La contraseña ingresada es inválida.",
        "ERROR_CONFIRMACION_CONTRASENA": "La contraseña y su confirmación, no coinciden.",
        "ERROR_FORMATO_CONTRASENA": "La contraseña debe ser alfanumérica  y de mínimo 6 caracteres.",
        "ERROR_CORREO_RECUPERACION_CONTRASENA": "Error al enviar el correo de recuperación de contraseña.",
        "ERROR_TOKEN_NO_VALIDO": "El token no es válido.",
        "TOKEN_INEXISTENTE_PETICION": "No se envió token en la petición.",
        "ERROR_DESCONOCIDO_CREAR_USUARIO": "Ha ocurrido un error desconocido al tratar de crear el usuario.",
        "ERROR_MIDDLEWARE_USUARIO" : "Se presento un error al autenticar el usuario contra el API.",
        "ERROR_SESION_INACTIVA": "Este usuario fue desactivado.",
        "ERROR_BUSCAR_SESION": "Error al buscar la sesión del usuario.",
        "ERROR_USUARIO_NO_ENCONTRADO": "Usuario no encontrado.",
        "ERROR_SESION_CORRUPTA": "La sesión está corrupta.",
        "ERROR_USUARIO_INACTIVO": "El usuario está inactivo en el sistema.",
        "ERROR_MODIFICAR_OTRO_USUARIO": "Su usuario no cuenta con los permisos para modificar otros usuarios.",
        "ERROR_USUARIO_SIN_SESION": "El usuario no está registrado en el portal.",
        "ERROR_CABECERA_SIN_TOKEN": "La petición no contiene en las cabeceras el token de autenticación.",
        "ERROR_PERMISOS_INSUFICIENTES": "Su usuario no cuenta con los permisos para ejecutar esta acción.",
        "ERROR_BUSCAR_USUARIO": "Ha ocurrido un error al buscar el usuario.",
        "ERROR_RECUPERACION_CONTRASENA": "Error al recuperar la contraseña.",
        "ERROR_CORREO_NO_ENCONTRADO": "No se encontro el usuario con el correo ingresado."
    },
    "rol": {
        "ERROR_ROL_INEXISTENTE": "Uno o maś de los roles asociados a la petición no existen o están inactivos en el sistema.",
    },
    "VALIDACIONES_CAMPOS" : {
        "CAMPOS_OBLIGARIOS" : "No se han ingresado todos los campos obligatorios.\n",
        "CABECERA_CAMPOS_OBLIGARIOS" : "El campo  es obligatorio",
        "FORMATO_CAMPO_INVALIDO" : "Uno o más campos tienen formato inválido.\n",
        "CABECERA_FORMATO_CAMPO_INVALIDO" : "Formato inválido",
        "MAXIMA_LONGITUD_CAMPO" : "Hay campos que superan la longitud definida.\n",
        "CABECERA_MAXIMA_LONGITUD_CAMPO" : "El campo supera el  límite de longitud máximo",
        "MINIMA_LONGITUD_CAMPO" : "Hay campos que no cumplen el límite de  longitud mínimo.\n",
        "CABECERA_MINIMA_LONGITUD_CAMPO" : "El campo no cumple el límite de longitud mínimo",
        "MINIM0_CAMPO_NUMERICO" : "Hay campos numéricos que no superan su límite de valor mínimo.\n",
        "CABECERA_MINIM0_CAMPO_NUMERICO" : "El campo no cumple el  límite de valor mínimo",
        "MAXIMO_CAMPO_NUMERICO" : "Hay campos numéricos que superan su límite de valor máximo.\n",
        "CABECERA_MAXIMO_CAMPO_NUMERICO" : "El campo supera el límite de valor máximo",
        "MAXIMO_POSICIONES_DECIMALES" : "Hay campos numéricos que superan el número de cifras decimales permitidas.\n",
        "CABECERA_MAXIMO_POSICIONES_DECIMALES" : "El campo supera el número de cifras decimales permitidas"
    },
    "general" : {
        "ERROR_SIN_TRAZA" : "Error sin traza.",
        "ERROR_SIN_MENSAJE" : "Error sin mensaje.",
        "ERROR_SIN_DETALLE" : "Error sin detalle."
    },
    "moongose":{
        "LLAVE_DUPLICADA" : "El campo '{PATH}' con el valor '{VALUE}' ya existe en el sistema.\n",
        "ERRORES_NATIVOS" : {
            "LLAVE_DUPLICADA" : "DuplicateKey"
        }
    }
}
