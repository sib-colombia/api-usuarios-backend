export default {
  "usuario": {
    "CREACION_USUARIO_SATISFACTORIA": "El usuario fue creado exitosamente.",
    "ACTUALIZACION_SATISFACTORIA": "El usuario fue  actualizado exitosamente.",
    "ACTUALIZACION_CONTRASENA_CORRECTA": "La contraseña fue actualizada exitosamente",
    "ENVIO_ENLACE_RECUPERACION": "Se envío un enlace a su correo electrónico para la recuperación de la contraseña.",
  },
  "general" : {
    "CABECERA_CORREO_RECUPERACIÓN_CONTRASEÑA": "Portal de Información de Tráfico Ilegal de Fauna Silvestre - Recuperacion contraseña",
    "NOT_LOGIN": "SIN AUTENTICACIÓN",
    "CABECERA_CORREO_GENERACION_REPORTE": "Portal de Información de Tráfico Ilegal de Fauna Silvestre - Descarga de reporte"
  }
}

