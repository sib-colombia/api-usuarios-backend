const message = (status, mensaje, response, ) => {

  const data = {};
  data.data = response;
  data.message = mensaje;

  return data;
};

const loggerAudit = (mensaje, usuario) => {
  const msg = mensaje+ " | " + usuario
  return msg;
};

const validatePassword =(password) => {
  if(/^[0-9a-zA-Z]+$/.test(password && password.length) && password.length > 5){
    return true
  }else{
    return false
  }
}

const putAuditoryData = (body, id) => {
  let entity = body
  if (id == null) {
    entity.user_create = httpContext.get('user')
    entity.create_date = new Date();
    entity.user_modify = httpContext.get('user')
    entity.modify_date = new Date();
  } else {
    entity.user_modify = httpContext.get('user')
    entity.modify_date = new Date();
  }
  return entidad
};

module.exports = { message , loggerAudit, validatePassword}
