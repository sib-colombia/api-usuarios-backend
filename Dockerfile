FROM node:12.18.1
WORKDIR /home/node/app
COPY ["package.json", "package-lock.json*", ".env", "./"]
RUN ["chmod", "+x", "/usr/local/bin/docker-entrypoint.sh"]
RUN npm install
COPY . .

