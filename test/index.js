/* global describe, it */

import http from 'http'
import assert from 'assert'
import request from 'request'
import config from '../src/config'
import _ from 'lodash'
import mongoose from 'mongoose'


var data = require('./data')

console.log(data)
console.log(_.repeat("=", 100))


var host = `http://${config.server.ip}:${config.server.port}`

console.log("La configuración es ", host)


function http_post(url, n) {
  return {
    url: host + url,
    method: "POST",
    json: true,
    body: n
  }
}


describe('/', () => {


  before((done) => {
    mongoose.connect('mongodb://192.168.11.92/api-usuarios-dev', () => {
      mongoose.connection.db.dropDatabase(() => {
        done()
      })
    })
  })


  const host = `http://${config.server.ip}:${config.server.port}`;

  // Server Online
  it('host should return 200 (Server Online)', done => {
    http.get(host, res => {
      assert.equal(200, res.statusCode);
      done();
    });
  });


  it('/api/user/me should return 403', done => {
    http.get(host + '/api/user/me', res => {
      assert.equal(500, res.statusCode)
      done()
    })
  })


  data.systems.forEach(function (test, key) {
    it('Crear sistemas ' + test.name + '', done => {
      request(http_post('/api/system', test), (error, res, body) => {
        //console.log(body)
        assert.notEqual(null, body._id)
        data.systems[key]._id = body._id
        done()
      })
    });
  });


  it('Ajusta ids de permissions', done => {
    //console.log("Ajustando permissions")
    data.permissions.forEach(function (p, key) {
      //console.log("Buscando ", data.permissions[key].system)
      //console.log("Encontrado", _.find(data.systems, {"name": data.permissions[key].system}))
      data.permissions[key].system = _.find(data.systems, {"name": data.permissions[key].system})._id
    })
    done()
  })

  data.permissions.forEach(function (test, key) {
    it('Crear permissions ' + test.name + '', done => {
      request(http_post('/api/permission', test), (error, res, body) => {
        //console.log(body)
        assert.notEqual(null, body._id)
        data.permissions[key]._id = body._id
        done()
      })
    });
  });

  it('Ajusta ids de roles', done => {
    //console.log("Ajustando roles")
    data.roles.forEach(function (r, key) {
      data.roles[key].system = _.find(data.systems, {"name": data.roles[key].system})._id

      let ps = []
      _.split(data.roles[key].permissions, ",").forEach(function (sp) {
        //console.log("Búscando permiso: ", sp)
        let p = _.find(data.permissions, {"name": sp})
        //console.log("Permiso: ", p)

        ps.push(p._id)
      })
      data.roles[key].permissions = ps
    })
    //console.log(data.roles)
    done()
  })

  data.roles.forEach(function (test, key) {
    it('Crear roles ' + test.name + '', done => {
      request(http_post('/api/rol', test), (error, res, body) => {
        //console.log(body)
        assert.notEqual(null, body._id)
        data.roles[key]._id = body._id
        done()
      })
    });
  });

  it('Ajusta ids de users', done => {
    //console.log("Ajustando users")
    data.users.forEach(function (u, key) {
      // console.log("Ajustando usuario: ", u.username)
      let rs = []
      _.split(data.users[key].roles, ",").forEach(function (sr) {
        //console.log("Búscando rol: ["+sr+"]")
        let r = _.find(data.roles, {"name": sr})
        // console.log("Rol: ", r._id)

        rs.push(r._id)
      })
      // console.log(rs)
      data.users[key].roles = rs
    })
    // console.log(data.users)
    done()
  })


  let adminCatalogo = null

  data.users.forEach(function (test, key) {
    it('Crear users ' + test.name + '', done => {

      test.lastname = test.name
      test.email = test.username + "@sib.com"
      test.password = "123456asda"
      test.verify = "123456asda"

      console.log("-------->"+JSON.stringify(test))
      // if (test.username === "adminCatalogo") {
      //   adminCatalogo = test
      // }

      request(http_post('/api/user', test), (error, res, body) => {
        console.log(body)
        assert.notEqual(null, body._id)
        done()
      })
    })
  })


  // data.listaTest.forEach(function(t, key) {
  //   it('Verificar permisos adminCatalogo catalogo.biodiversidad.co/ficha '+t.test.action+" resultado esperado "+t.response , done => {
  //     request(http_post('/api/user/permission', t.test), (error, res, body) => {
  //       //console.log(body)
  //       assert.equal(t.response, body.response)
  //       done()
  //     })
  //   })
  // })
  //
  //
  // it('Login users ', done => {
  //   let login = {"username":"adminCatalogo","password":"123456","system":"CATALOGO"}
  //   console.log(login)
  //   request(http_post('/auth/local', login), (error, res, body) => {
  //     console.log(body)
  //     done()
  //   })
  // })


  data.users.forEach(function (u, key) {
    u.roles.split(",").forEach(function (r, key) {
      data.roles.forEach(function (r2, key) {

        if (r === r2.name) {
          it('Login users [' + u.username + "]   system:  [" + r2.system + "]", done => {

            data.systems.forEach(function (s, key) {
              if (s._id === r2.system) {
                let login = {"username": u.username, "password": "123456", "system": s.name}
                request(http_post('/auth/local', login), (error, res, body) => {
                  console.log(body)
                  done()
                })
              }
            })
          })
        }
      })
    })
  })


// Probar el login de este sistema en el otro sistema
// Probar los permisos de login del otro sistema


});
