var systems = [
  {
    "name": "DOMINIOS",
    "description": "Dominios de Datos del SiB Colombia",
    "url": "dominios.biodiversidad.co",
    "active": true
  },
  {
    "name": "USUARIOS",
    "description": "Portal de Usuarios del SiB Colombia",
    "url": "apiuser.biodiversidad.co",
    "active": true
  },
  {
    "name": "PIFS",
    "description": "Portal de Tráfico de Especies",
    "url": "betapifs.biodiversidad.co",
    "active": true
  }
];

var permissions = [
  {
    "name": "CREAR AUTORIDAD AMBIENTAL",
    "description": "Crear una nueva autoridad ambiental.",
    "url": "/api/autoridadAmbiental",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "MODIFICAR AUTORIDAD AMBIENTAL",
    "description": "Modificar una autoridad ambiental existente.",
    "url": "/api/autoridadAmbiental/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "BUSCAR AUTORIDAD AMBIENTAL",
    "description": "Consultar autoridad (es) ambiental(es) según criterios de búsqueda.",
    "url": "/api/autoridadAmbiental/buscar",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CONSULTAR AUTORIDAD AMBIENTAL",
    "description": "Consultar una autoridad ambiental por id.",
    "url": "/api/autoridadAmbiental/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": true,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CREAR CONTACTO",
    "description": "Crear un nuevo contacto.",
    "url": "/api/contacto",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "MODIFICAR CONTACTO",
    "description": "Modificar contacto existente.",
    "url": "/api/contacto/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "BUSCAR  CONTACTO",
    "description": "Buscar contacto(s) según criterios de bùsqueda.",
    "url": "/api/contacto/buscar",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CONSULTAR CONTACTO",
    "description": "Consultar un contacto por id.",
    "url": "/api/contacto/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": true,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CREAR ENTIDAD A DISPOSICIÓN",
    "description": "Crear una nueva entidad de disposición.",
    "url": "/api/entidadDisposicion",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "MODIFICAR  ENTIDAD A DISPOSICIÓN",
    "description": "Modificar una entidad de disposición existente.",
    "url": "/api/entidadDisposicion/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "BUSCAR ENTIDAD A DISPOSICIÓN",
    "description": "Buscar entidad (es) de disposición según criterios de búsqueda.",
    "url": "/api/entidadDisposicion/buscar",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CONSULTAR ENTIDAD A DISPOSICIÓN",
    "description": "Consultar una entidad de disposición por id.",
    "url": "/api/entidadDisposicion/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": true,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CREAR EXPEDIENTES",
    "description": "Crear un nuevo expediente.",
    "url": "/api/expediente",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "MODIFICAR EXPEDIENTE",
    "description": "Modicar un expediente por id.",
    "url": "/api/expediente/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "RADICAR  EXPEDIENTE",
    "description": "Radicar un expediente por id.",
    "url": "/api/expediente/radicar/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "CERRAR EXPEDIENTE",
    "description": "Cerrar un expediente por id.",
    "url": "/api/expediente/cerrar/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "BUSCAR EXPEDIENTE",
    "description": "Buscar expediente (s) según criterios de búsqueda.",
    "url": "/api/expediente/buscar/:offset/:size",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CONSULTAR EXPEDIENTE",
    "description": "Consultar un expediente por id.",
    "url": "/api/expediente/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": true,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "MODIFICAR ESPECÍMENES DE EXPEDIENTE",
    "description": "Crear, modificar o remover especímenes en un expedientes usando el id de este último.",
    "url": "/api/expediente/:id/especimenes",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "CREAR TRASLADO",
    "description": "Crea un nuevo traslado.",
    "url": "/api/traslado",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "RESPONDER TRASLADO",
    "description": "Aprobar o rechazar un traslado usando el id.",
    "url": "/api/traslado/aprobar/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "CANCELAR TRASLADO",
    "description": "Cancelar un traslado usando el id.",
    "url": "/api/traslado/cancelar/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "BUSCAR TRASLADO",
    "description": "Buscar traslado (s) según criterios de búsqueda.",
    "url": "/api/traslado/buscar/:offset/:size",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CONSULTAR TRASLADO",
    "description": "Consultar un traslado por id.",
    "url": "/api/traslado/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": true,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "MODIFICAR ESPECÌMENES DE TRASLADO",
    "description": "Crear, modificar o remover especímenes en un traslado usando el id de este último.",
    "url": "/api/traslado/:id/especimenes",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "BUSCAR ESPECÍMEN",
    "description": "Buscar especímen(es) de un expediente según criterios de búsqueda.",
    "url": "/api/especimen/expediente/buscar",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CONSULTAR ESPECÍMEN",
    "description": "Consultar especímen por id",
    "url": "/api/especimen/expediente/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": true,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "BUSCAR  ESPECÍMEN  DE TRASLADO",
    "description": "Buscar especímen(es) de un traslado según criterios de búsqueda.",
    "url": "/api/especimen/traslado/buscar",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CONSULTAR  ESPECÍMEN  DE TRASLADO",
    "description": "Consultar el especímen de un traslado por id.",
    "url": "/api/especimen/traslado/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": true,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CREAR ARCHIVO DE EXPEDIENTE",
    "description": "Crear un nuevo archivo para un expediente.",
    "url": "/api/archivo/expediente",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "MODIFICAR ARCHIVO DE EXPEDIENTE",
    "description": "Modificar un archivo de expediente por id.",
    "url": "/api/archivo/expediente",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "CONSULTAR ARCHIVO EXPEDIENTE",
    "description": "Consuta un archivo de expediente por id.",
    "url": "/api/archivo/expediente/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": true,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CREAR  ARCHIVO DE TRASLADO",
    "description": "Crear un nuevo archivo para un traslado.",
    "url": "/api/archivo/traslado",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "MODIFICAR  ARCHIVO DE TRASLADO",
    "description": "Modificar un archivo de traslado por id.",
    "url": "/api/archivo/traslado",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "CONSULTAR ARCHIVO  DE TRASLADO",
    "description": "Consuta un archivo de traslado por id.",
    "url": "/api/archivo/traslado/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": true,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CREAR  ARCHIVO DE ENTIDAD",
    "description": "Crear un nuevo archivo para una entidad.",
    "url": "/api/archivo/entidad",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "MODIFICAR  ARCHIVO DE ENTIDAD",
    "description": "Modificar un archivo de entidad por id.",
    "url": "/api/archivo/entidad",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "CONSULTAR ARCHIVO  DE ENTIDAD",
    "description": "Consuta un archivo de entidad por id.",
    "url": "/api/archivo/entidad/:id",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": true,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CREAR IMAGEN EXPEDIENTE",
    "description": "Crear una nueva imagen para un expediente.",
    "url": "/api/imagen/expediente",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "MODIFICAR IMAGEN EXPEDIENTE",
    "description": "Modificar una imagen de expediente por id.",
    "url": "/api/imagen/expediente",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "CONSULTAR IMAGEN EXPEDIENTE",
    "description": "Consuta una imagen de expediente por id.",
    "url": "/api/imagen/:id/expediente",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": true,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CREAR USUARIO API PIFS",
    "description": "Crear un nuevo usuario en API Pifs",
    "url": "/api/usuario/crear",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "MODIFICAR USUARIO API PIFS",
    "description": "Modificar la información asociada a un contacto.",
    "url": "/api/usuario/actualizar/:usuario",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "LISTAR USUARIOS API PIFS",
    "description": "Listar los usuarios registrados en API Pifs.",
    "url": "/api/usuario/listar",
    "active": true,
    "system": "PIFS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "CREAR USUARIO API USUARIOS",
    "description": "Crear usuarios en API Usuarios.",
    "url": "/api/user",
    "active": true,
    "system": "USUARIOS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "MODIFICAR USUARIO API USUARIOS",
    "description": "Modificar usuarios en API Usuarios.",
    "url": "/api/user",
    "active": true,
    "system": "USUARIOS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "MODIFICAR USUARIO COMO ADMIN API USUARIOS",
    "description": "Modificar atributos de usuarios que solo pueden ser modificados por un administrador.",
    "url": "/api/admin/user/:username",
    "active": true,
    "system": "USUARIOS",
    "actions":
        {
          "post": false,
          "get": false,
          "put": true,
          "delete": false,
        }
  },
  {
    "name": "CONSULTAR UN USUARIO",
    "description": "Consultar los datos referentes a un usuario.",
    "url": "/api/user/:username",
    "active": true,
    "system": "USUARIOS",
    "actions":
        {
          "post": false,
          "get": true,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "OBTENER INFORMACIÓN DE SESIÓN DE USUARIO",
    "description": "Obtener información del usurio logueado en sesión",
    "url": "/api/user/me",
    "active": true,
    "system": "USUARIOS",
    "actions":
        {
          "post": false,
          "get": true,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "LISTAR USUARIOS POR SISTEMA",
    "description": "Lista los uusarios de un sistema",
    "url": "/api/user/find/system",
    "active": true,
    "system": "USUARIOS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "OBTENER TOKEN DE MODIFICACIÓN DE CONTRASEÑA",
    "description": "Obtener enlace para la recuperación de la contraseña.",
    "url": "/api/user/password/recovery",
    "active": true,
    "system": "USUARIOS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  },
  {
    "name": "RECUPERAR CONTRASEÑA",
    "description": "Modificar la contraseña cn base al token creado por el sistema.",
    "url": "/api/user/password/token",
    "active": true,
    "system": "USUARIOS",
    "actions":
        {
          "post": true,
          "get": false,
          "put": false,
          "delete": false,
        }
  }
];

var roles = [
  {
    "name": "ADMINISTRADOR DEL SISTEMA",
    "description": "Rol que administra el sistema. Se encarga de la administración de roles y permisos en la aplicación, las listas de valores y, junto con el coordinador de registros, administran las autoridades ambientales.",
    "permissions": "CREAR AUTORIDAD AMBIENTAL,MODIFICAR AUTORIDAD AMBIENTAL,BUSCAR AUTORIDAD AMBIENTAL,CONSULTAR AUTORIDAD AMBIENTAL,BUSCAR EXPEDIENTE,CONSULTAR EXPEDIENTE,BUSCAR ESPECÍMEN,CONSULTAR ESPECÍMEN,CREAR USUARIO API PIFS,MODIFICAR USUARIO API PIFS,LISTAR USUARIOS API PIFS",
    "active": true,
    "system": "PIFS"
  },
  {
    "name": "COORDINADOR DE AUTORIDAD AMBIENTAL",
    "description": "Rol encargado de la administración del modulo de autoridad ambiental y los contactos asociados a estas.",
    "permissions": "CREAR CONTACTO,MODIFICAR CONTACTO,BUSCAR  CONTACTO,CONSULTAR CONTACTO,CREAR ENTIDAD A DISPOSICIÓN,MODIFICAR  ENTIDAD A DISPOSICIÓN,BUSCAR ENTIDAD A DISPOSICIÓN,CONSULTAR ENTIDAD A DISPOSICIÓN",
    "active": true,
    "system": "PIFS"
  },
  {
    "name": "COORDINADOR DE EXPEDIENTES",
    "description": "Encargado de la administración de los expedientes sujetos de tráfico gestionados en el sistema, incluye la creación, revisión y cierre del mismo,  adicionalmente administra la expedición de salvoconductos.",
    "permissions": "CREAR EXPEDIENTES,MODIFICAR EXPEDIENTE,RADICAR  EXPEDIENTE,CERRAR EXPEDIENTE,BUSCAR EXPEDIENTE,CONSULTAR EXPEDIENTE,MODIFICAR ESPECÍMENES DE EXPEDIENTE,CREAR TRASLADO,CANCELAR TRASLADO,BUSCAR TRASLADO,CONSULTAR TRASLADO,MODIFICAR ESPECÌMENES DE TRASLADO,BUSCAR ESPECÍMEN,CONSULTAR ESPECÍMEN,BUSCAR  ESPECÍMEN  DE TRASLADO,CONSULTAR  ESPECÍMEN  DE TRASLADO,CREAR ARCHIVO DE EXPEDIENTE,MODIFICAR ARCHIVO DE EXPEDIENTE,CONSULTAR ARCHIVO EXPEDIENTE,CREAR  ARCHIVO DE TRASLADO,MODIFICAR  ARCHIVO DE TRASLADO,CONSULTAR ARCHIVO  DE TRASLADO,CREAR IMAGEN EXPEDIENTE,MODIFICAR IMAGEN EXPEDIENTE,CONSULTAR IMAGEN EXPEDIENTE",
    "active": true,
    "system": "PIFS"
  },
  {
    "name": "COORDINADOR DE ENTIDADES DE DISPOSICIÓN",
    "description": "Encargado de la administración de los catálogos de entidades de disposición y centros de atención y valoración.",
    "permissions": "CREAR  ARCHIVO DE ENTIDAD,MODIFICAR  ARCHIVO DE ENTIDAD,CONSULTAR ARCHIVO  DE ENTIDAD,BUSCAR TRASLADO,CONSULTAR TRASLADO,RESPONDER TRASLADO,BUSCAR  ESPECÍMEN  DE TRASLADO,CONSULTAR  ESPECÍMEN  DE TRASLADO,CONSULTAR ARCHIVO  DE TRASLADO",
    "active": true,
    "system": "PIFS"
  },
  {
    "name": "USUARIO PIFS GENÉRICO",
    "description": "Usuario que contiene los permisos mínimos para que un usuario cualquiera gestione su cuenta.",
    "permissions": "MODIFICAR USUARIO API USUARIOS,CONSULTAR UN USUARIO,OBTENER INFORMACIÓN DE SESIÓN DE USUARIO,OBTENER TOKEN DE MODIFICACIÓN DE CONTRASEÑA,RECUPERAR CONTRASEÑA",
    "active": true,
    "system": "USUARIOS"
  },
  {
    "name": "ADMINISTRADOR USUARIOS SISTEMA",
    "description": "Administrador de los usuarios de un sistema.",
    "permissions": "CREAR USUARIO API USUARIOS,MODIFICAR USUARIO COMO ADMIN API USUARIOS,LISTAR USUARIOS POR SISTEMA",
    "active": true,
    "system": "USUARIOS"
  }
];

var users = [
  {
    "username": "ADMINISTRADORPIFS",
    "name": "ADMINISTRADORPIFS",
    "roles": "ADMINISTRADOR DEL SISTEMA,ADMINISTRADOR USUARIOS SISTEMA,USUARIO PIFS GENÉRICO"
  },
  {
    "username": "COORDINADORAUTORIDAD",
    "name": "COORDINADORAUTORIDAD",
    "roles": "COORDINADOR DE AUTORIDAD AMBIENTAL,USUARIO PIFS GENÉRICO"
  },
  {
    "username": "COORDINADOREXPEDIENTES",
    "name": "COORDINADOREXPEDIENTES",
    "roles": "COORDINADOR DE EXPEDIENTES,USUARIO PIFS GENÉRICO"
  },
  {
    "username": "COORDINADORENTIDADES",
    "name": "COORDINADORENTIDADES",
    "roles": "COORDINADOR DE ENTIDADES DE DISPOSICIÓN,USUARIO PIFS GENÉRICO"
  }
];


let listaTest = [
  {
    test: {
      "username": "COORDINADOREXPEDIENTES",
      "url": "/expediente/:id",
      "action": "put"
    },
    response: true
  },
];

module.exports = {
  systems: systems,
  permissions: permissions,
  roles: roles,
  users: users,
  listaTest: listaTest,
};

